rootProject.name = "TestSandbox"

pluginManagement {
    repositories {
        gradlePluginPortal()
        jcenter()
    }
}
