package org.sandbox

/**
 * Integer addition
 * @param a1 First param
 * @param a2 Second param
 * @return addition
 */

fun add(a1: Int, a2: Int) = a1 + a2
